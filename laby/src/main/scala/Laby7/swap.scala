@main
def swap(): Unit = {
    println(swapHelper(List(1, 2, 3, 4, 5)))
}

def swapHelper[A](l: List[A]): List[A] = {
    return l.sliding(2, 2).map((pair) => {
    if(pair.size == 2){
        List(pair(1), pair(0))
    }
    else{
        pair
    }  
})
    .toList.foldLeft(List[A]())((acc, value) => {
        acc ::: value
    })
}
