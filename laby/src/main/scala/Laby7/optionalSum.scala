@main
def optionalSum(): Unit = {
    val lista = List(Some(5.4), Some(-2.0), Some(1.0), None, Some(2.6))
    println(sumOpts(lista))
    println(sumOpts(List()))
    println(sumOpts(List(None)))
}

def sumOpts(list: List[Option[Double]]): Option[Double] = {
    return list.foldLeft(Option.empty[Double])((acc, current) => {(current, acc) match{
        case (None, acc) => {acc}
        case (Some(x), None) => {Some(x)}
        case (Some(x), Some(y)) => {Some(x + y)}
    }})
}