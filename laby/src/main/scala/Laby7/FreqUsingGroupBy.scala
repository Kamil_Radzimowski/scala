@main
def Freq() : Unit = {
    println(frequency(List(1, 1, 2, 3, 3, 3 , 4)))
}

def frequency[A](list: List[A]): List[(A, Int)] = {
    return list.groupBy((item) => item).toList.map((item) => {(item(0), item(1).size)})
}