@main
def indices(): Unit = {
    println(ind(List(1, 2, 1, 1, 3), 1))
}

def ind[A](l: List[A], el: A): Set[Int] = {
    return l.zipWithIndex.foldLeft(List[Int]()){(acc, value) => {
        (acc, value) match{
            case(acc, (x, y)) if (x == el) => {
                acc :+ y
            }
            case (_, _) =>{
                acc
            }
        }
    }}.toSet
}