@main
def strip(): Unit = {
    val strefy: List[String] = java.util.TimeZone.getAvailableIDs.toList
    println(getEuropeanTimeZones(strefy))
}

def getEuropeanTimeZones(list: List[String]): List[String] = {
    return list.filter((zone) => {zone.contains("Europe")}).map((zone) => zone.stripPrefix("Europe/")).sortBy(zone => (zone.size, zone))
}