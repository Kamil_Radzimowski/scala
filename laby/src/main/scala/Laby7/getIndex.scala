@main
def getIndex(): Unit = {
    val lista = List(2, 1, 1, 5)
    println(position(lista, 1)) // ==> Some(1)
    println(position(lista, 3)) // ==> None
}

def position[A](list: List[A], el: A): Option[Int] = {
    return list.zipWithIndex.foldLeft(Option.empty[Int])((acc, current) => {
        (acc, current) match {
            case (None, (x, y)) if(x == el) => {
                return Some(y)
            }
            case (accum , _) => {
                accum
            }
        }
    })
}