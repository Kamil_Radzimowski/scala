import scala.annotation.tailrec

@main
def divide(): Unit = {
    val list = List(1, 2, 3, 4, 5, 6)
    println(divide_func(list))
}


def divide_func[A](l: List[A]): (List[A], List[A]) = {

    @tailrec
    def div(list: List[A], index: Int, even: List[A], odd: List[A]): (List[A], List[A]) = {
        list match {
            case Nil => (odd, even)
            case value :: tail => if(index % 2 == 0){div(tail, index + 1, even :+ list.head, odd)}else{div(tail, index + 1, even, odd :+ list.head)}
        }
    }
    return div(l, 0, List(), List())
}
