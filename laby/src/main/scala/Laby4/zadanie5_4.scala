type Pred[A] = A => Boolean

@main
def predykat(): Unit = {
    val wiekszaNiz10 = (n: Int) => n > 10
    val mniejszaNiz5 = (n: Int) => n < 5
    val fun = and(wiekszaNiz10, wiekszaNiz10)
    val fun_3 = not(wiekszaNiz10)
    val fun_4 = imp(wiekszaNiz10, mniejszaNiz5)
    println(fun_4(9))
}


def and[A](p: Pred[A], q: Pred[A]): Pred[A] = {
    a => p(a) && q(a)
}


def or[A](p: Pred[A], q: Pred[A]): Pred[A] = {
    a => p(a) || q(a)
}

def not[A](p: Pred[A]): Pred[A] = {
    a => !p(a)
}

def imp[A](p: Pred[A], q: Pred[A]): Pred[A] = {
    a => !p(a) || q(a)
}