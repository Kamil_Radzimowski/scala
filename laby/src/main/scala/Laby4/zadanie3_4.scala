import scala.annotation.tailrec

@main
def deletion() : Unit = {
    val lista = List(1, 2, 3, 3, 4, 5)

    println(usun(lista, 3))
}

def usun[A](l: List[A], el: A): List[A] = {
    
    @tailrec
    def helper[A](l: List[A], el: A, acc: List[A]): List[A] = {
        l match {
            case Nil => acc
            case value :: tail => if(value == el){helper(tail, el, acc)}else{helper(tail, el, acc :+ value)}
        }
    }

    return helper(l, el, List())
}