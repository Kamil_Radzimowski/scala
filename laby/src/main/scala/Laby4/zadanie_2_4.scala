import scala.annotation.tailrec

@main
def maksimum(): Unit = {
    val lista = List(4.0, 2.0, 3.5)
    val list2 = List(5.0, 1.0, 4.0)

    println(max(lista, list2))
}

def max(l1: List[Double], l2: List[Double]) : List[Double] = {

    @tailrec
    def helper_2(l1: List[Double], l2: List[Double], acc: List[Double]): List[Double] = {
        (l1, l2) match {
            case (Nil, Nil) => acc
            case (Nil, x :: tail) => helper_2(l1, tail, acc :+ x)
            case (x :: tail, Nil) => helper_2(tail, l2, acc :+ x)
            case (x :: tail1, y :: tail2) => if(x > y){helper_2(tail1, tail2, acc :+ x)}else{helper_2(tail1, tail2, acc :+ y)} 
        }
    }

    return helper_2(l1, l2, List())
}

