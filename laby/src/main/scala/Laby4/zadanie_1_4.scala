import scala.annotation.tailrec

@main
def sumuj(): Unit = {
    val lista = List(Some(4.0), Some(-3.0), None, Some(1.0))
    println(sum(lista))
}

@tailrec
def helper(l: List[Option[Double]], acc: Double): Option[Double] = {
    l match {
        case Nil => if(acc > 0){Some(acc)}else {None}
        case Some(x) :: ogon => if(x>0){helper(ogon, acc+x)}else{helper(ogon, acc)}
        case _ :: ogon => helper(ogon, acc)
    }
}



def sum(l: List[Option[Double]]): Option[Double] = {
    return helper(l, 0.0)
}



