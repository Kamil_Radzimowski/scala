package Laby.Laby1

object Zadanie1 {
    def main(args: Array[String]): Unit = {
        val x  = scala.io.StdIn.readInt()
        if (x%2==0){
          println("Liczba parzysta")
        }
        else{
          println("Liczba nie parzysta")
        }
    }
}
