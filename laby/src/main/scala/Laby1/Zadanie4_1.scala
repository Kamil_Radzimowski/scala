package Laby.Laby1
import Zadanie3.isPrime

import scala.annotation.tailrec

object Zadanie4 {
  def main(args: Array[String]): Unit = {
    val x = scala.io.StdIn.readInt()
    println(isSumOfTwoPrimeNums(x))
  }

  def isSumOfTwoPrimeNums(x: Int): Boolean = {
    isSumOfTwoPrimeNumsHelper(x, 2)
  }

  @tailrec
  private def isSumOfTwoPrimeNumsHelper(x: Int, counter: Int): Boolean = {
    if(counter > (x/2)){
      return false
    }
    if(isPrime(counter)){
      if(isPrime(x - counter)){
        println(String.format("Liczba jest sumą: %d+%d=%d", counter, x - counter, x))
        return true
      }
    }
    isSumOfTwoPrimeNumsHelper(x, counter + 1)
  }
}
