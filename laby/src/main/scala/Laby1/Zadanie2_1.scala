package Laby.Laby1

import scala.annotation.tailrec

object Zadanie2 {
  def main(args: Array[String]):Unit = {
      println(NWD(10, 5))
      println(NWD(5, 10))
      println(NWD(4, 8))
  }

  @tailrec
  def NWD(x: Integer, y:Integer):Integer = {
    if (x==y){
      x
    }
    else if(x > y){
      NWD(x - y, y)
    }
    else{
      NWD(x, y - x)
    }
  }
}
