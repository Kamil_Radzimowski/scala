def obramuj(napis: String): String = {
    var result = ""
    var arr =  napis.split('\n')
    var longest = getTheLongestWordCharCount(arr)
    result = result + drawStraightLine(longest)
    result = result + drawTabLine(longest)
    result = result + drawText(arr, longest)
    result = result + drawTabLine(longest)
    result = result + drawStraightLine(longest)
    return result
}

def drawTextHelper(arr: Array[String], currentIndex: Int, longest: Int): String = {
    var result = ""
    if (currentIndex >= arr.length){
        return result
    }
    result = result + ("*")
    result = result + (" ")
    result = result + (arr(currentIndex))
    result = result + (" " * (longest - arr(currentIndex).length()))
    result = result + (" ")
    result = result + ("*")
    result = result + ("\n")
    return result + drawTextHelper(arr, currentIndex + 1, longest)
}

def drawText(arr: Array[String], longest: Int): String = {
    drawTextHelper(arr, 0, longest)
}

def drawStraightLine(len: Int): String = {
    var napis = ""
    napis = napis + ("*" * (len + 4))
    napis = napis + "\n"
    return napis
}

def drawTabLine(len: Int): String = {
    var napis = ""
    napis = napis + ("*")
    napis = napis + (" " * (len + 2))
    napis = napis + ("*")
    napis = napis + "\n"
    return napis
}

def countNumberOfNewLines(napis: String): Int = {
    napis.count(_ == '\n')
}

def getTheLongestWordCharCount(arr: Array[String]): Int = {
    def helper(arr: Array[String], currentMax: Int, currentIndex: Int): Int = {
        if(currentIndex >= arr.length){
            return currentMax
        }
        if (arr(currentIndex).length() > currentMax){
            helper(arr, arr(currentIndex).length(), currentIndex + 1)
        }
        else{
            helper(arr, currentMax, currentIndex + 1)
        }
    }
    helper(arr, 0, 0)
}


@main 
def zad_01: Unit = {
    val argument = "ALA\nMA\nKOTAAAAAA"
    var wynik = obramuj(argument)
    println(wynik)
}


@main
def zad_02: Unit = {
    println("ZASZYFRUJ MNIE" + "KATAR")
}

/*
def zaszyfruj(slowo: String, klucz: String): String = {
    var matrix = createMatrix()
    if(slowo.length() > klucz.length()){
        klucz = makeWordLonger(klucz, slowo.length)
    }
    wypisz(matrix(1))
    return "f"
}
def makeWordLonger(word: String, len: Int): String = {

    def helper(word: String, len: Int, currentIndex: Int, currentWord: String): String = {
        if (currentWord.length == len){
            return currentWord
        }
        
    }
    return helper(word, len, 0, word)
}

def wypisz(arr: Array[Char]): Unit = {
    def helper(arr: Array[Char], currentIndex: Int): Unit = {
        if (currentIndex >= arr.length){
            return
        }
        println("Arr: " + arr(currentIndex))
        helper(arr, currentIndex + 1)
    }
    helper(arr, 0)
}

def createMatrix(): Array[Array[Char]] = {
    var arr = Array.ofDim[Char](26, 26)
    for (i <- 0 to 25){
        for (j <- 0 to 25){
            var let = i + j

            if (let >= 26){
                let = let - 26
            }
            let = let + 65
            var letter: Char = (let).toChar
            arr(i)(j) = letter
        }
    }
    return arr
}
*/


