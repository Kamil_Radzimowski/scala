@main
def usun_k_elem(): Unit ={
    println(remElems(List(1, 2, 3, 4, 5, 6), 2))
}

def remElems[A](list: List[A], k: Int): List[A] = {
    return list.zipWithIndex.filter((x) => {
        x(1) != k
    }).map(
        (x) => {
            x(0)
        }
    )
}