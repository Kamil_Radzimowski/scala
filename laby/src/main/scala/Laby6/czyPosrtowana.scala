@main
def jestPosortowana(): Unit = {
    val seq = Seq(1, 2, 2, 4)
    println(ordered(seq)(_ < _)) // ==> false
    println(ordered(seq)(_ <= _)) // ==> true
}


def ordered[A](seq: Seq[A])(leq: (A, A) => Boolean): Boolean = {
    return seq.sliding(2, 1).forall(x => {leq(x(0), x(1))})
}