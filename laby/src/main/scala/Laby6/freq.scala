@main
def czestotliwosc(): Unit = {
    println(freq(List(1, 1, 2, 4, 4, 3, 4, 1, 3)))
}

def freq[A](l: List[A]): (Set[A], Int) = {
    return l.foldLeft(List[(A , Int)]())((acc, current) => {
        acc :+ (current, l.count(x => {x == current}))
    }).foldLeft((Set[A](), 0))((max, item) => {
        (max, item) match {
            case ((max, count), (item, itemCount)) => {
                if(max.isEmpty){
                    (Set(item), itemCount)
                }
                else if(count < itemCount){
                    (Set(item), itemCount)
                }
                else if (count == itemCount){
                    (max + item, count)
                }
                else{
                    (max, count)
                }
            }
        }
    })
}