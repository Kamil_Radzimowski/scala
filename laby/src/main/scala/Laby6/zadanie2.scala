
@main
def pary(): Unit = {
    println(pairPosNeg(List(1, 0, -3, 4, 5, 6)))
}

def pairPosNeg(list: List[Double]): (List[Double], List[Double]) = {
    val (list1, list2) = list.partition(x => {x > 0})
    return  (list1.filter(x => {x != 0.0}), list2.filter(x => {x != 0.0}))
}