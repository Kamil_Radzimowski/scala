
@main
def subseq(): Unit = {
    println(sub(List(1, 2, 3, 4, 5, 6), 1, 5))
}

def sub[A](list: List[A], begIdx: Int, endIdx: Int): List[A] = {
    return list.take(endIdx).drop(begIdx)
}
