@main
def usun_powtorzenia(): Unit = {
    println(odstuteruj(List(1, 2, 2, 3, 4, 4, 1)))
}

def odstuteruj[A](list: List[A]): List[A] = {
    return list.foldLeft(List[A]())( (acc, a) => {
        acc match {
            case Nil => {a +: acc}
            case head :: tail => {
                if (head != a){
                  a +: acc
                }
                else{
                acc
                }
            }
        }
    }).reverse
}