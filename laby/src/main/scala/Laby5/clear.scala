import scala.annotation.tailrec

@main
def clear(): Unit = {
    println(oczysc(List(1, 2, 3, 3, 4, 4, 1)))
}


def oczysc[A](l: List[A]): List[A] = {

    @tailrec
    def helper(l: List[A],  acc: List[A]): List[A] = {
        l match {
            case Nil => acc.reverse
            case head :: tail => {
                acc match {
                    case Nil => helper(tail, head :: acc)
                    case accHead :: accTail =>{
                        if(accHead != head){
                            helper(tail, head :: acc)
                        }
                        else{
                            helper(tail, acc)
                        }
                    }
                }
            }
        }
    }
    return helper(l, List())
}