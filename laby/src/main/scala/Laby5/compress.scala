import scala.annotation.tailrec


@main
def compress() : Unit = {
    println(kompresuj(List('a', 'a', 'b', 'c', 'a', 'd', 'd')))
}

def kompresuj[A](l: List[A]): List[(A, Int)] = {
    
    @tailrec
    def helper[A](l: List[A], acc: List[(A, Int)]): List[(A, Int)] = {
        l match {
            case Nil => acc.reverse
            case head :: tail => {
                acc match {
                    case Nil => helper(tail, (head, 1) :: acc)
                    case accHead :: accTail => {
                        if(head == accHead(0)){
                            helper(tail, (accHead(0), accHead(1) + 1) :: accTail)
                        }
                        else{
                            helper(tail, (head, 1) :: acc)
                        }
                    }
                }
            }
        }
    }
    helper(l, List())
}