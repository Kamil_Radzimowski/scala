import scala.annotation.tailrec

@main
def isOrdered() : Unit = {
    println(isIt(List(1, 2, 2, 5))(_ < _))
    println(isIt(List(1, 2, 2, 5))(_ <= _))
}

def isIt[A](l: List[A])(leq: (A, A) => Boolean): Boolean = {

    @tailrec
    def helper[A](l: List[A], leq: (A, A) => Boolean, acc: List[A]): Boolean = {
        l match {
            case Nil => true
            case head :: tail => {
                acc match {
                    case Nil => helper(tail, leq, head :: acc)
                    case accHead :: accTail => {
                        if(leq(accHead, head)){
                            helper(tail, leq, head :: acc)
                        }
                        else{
                            false
                        }
                    }
                }
            }
        }
    }
    return helper(l, leq, List())
}
