import scala.annotation.tailrec


@main 
def applyForAll(): Unit = {
    println(applyWithMap(List(1, 2, 3))((n) => n + 3))
}

def apply[A, B](l: List[A])(f: A => B): List[B] = {

    @tailrec
    def helper(l: List[A], f: A => B, acc: List[B]): List[B] = {
        l match {
            case Nil => acc
            case head :: tail => {
                helper(tail, f, acc :+ f(head))
            }
        }
    }
    return helper(l, f, List())
}

def applyWithMap[A, B](l: List[A])(f: A => B): List[B] = {
    return l.map(
        f
    )
}