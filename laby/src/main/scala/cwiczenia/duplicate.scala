@main
def duplicate() : Unit = {
    println(duplic(List(1, 2, 3), 3))
}

def duplic[A](l: List[A], howManyTimes: Int): List[A] = {
    def helper(l: List[A], howManyTimes: Int, current: Int, acc: List[A]): List[A] = {
        l match {
            case Nil => acc
            case head :: tail => {
                if(current < howManyTimes){
                    helper(l, howManyTimes, current + 1, acc :+ head)
                }
                else{
                    helper(tail, howManyTimes, 0, acc)
                }
            }
        }
    }
    return helper(l, howManyTimes, 0, List())
}