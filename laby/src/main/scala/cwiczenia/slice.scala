@main
def slice() : Unit ={
    println(podziel(1, 3, List(1, 2, 3, 4, 5, 6, 7)))
}

def podziel[A](start: Int, end: Int, l: List[A]) : List[A] = {

    def helper[A](start: Int, end: Int, l: List[A], acc: List[A], currentIndex: Int): List[A] = {
        l match {
            case Nil => acc
            case head :: tail => {
                if(currentIndex >= start && currentIndex <= end){
                    helper(start, end, tail, acc :+ head, currentIndex + 1)
                }
                else{
                    helper(start, end, tail, acc, currentIndex + 1)
                }
            }
        }
    }
    return helper(start, end, l, List(), 0)
}