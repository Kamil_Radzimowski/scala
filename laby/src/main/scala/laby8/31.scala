@main
def ranking(): Unit = {
    val entryData = List(("Kamil", "Radzimowski", 5, 3), ("Kamil", "Radzimowski", 3, 4), ("Kamil", "Testowy", 6, 4), ("Kamil", "Testowy", 1, 2), ("Kamil", "Testowy", 6, 1))
    println(rank(entryData))
}

def rank(l: List[(String, String, Int, Int)]): List[(Int, String, String, Double)] = {
    val results = l.foldLeft(List[(String, String, Double, Double)]())((acc, current) => {
        val filtered = l.filter((item) => {item(0) == current(0) && item(1) == current(1)})
        val ocena_w = filtered.foldLeft(0.0)((acc_w, cur_w) => {acc_w + cur_w(2)}) / filtered.length
        val ocena_s = filtered.foldLeft(0.0)((acc_s, cur_s) => {acc_s + cur_s(3)}) / filtered.length
        acc :+ (current(0), current(1), ocena_w, ocena_s)
    })
    val result_sum = results.foldLeft(List[(String, String, Double)]())((acc, current) => {
        acc :+ (current(0), current(1), current(2) + current(3))
    })
    return result_sum.distinct.sortBy(x => (-x(2), x(1))).zipWithIndex.map((item) => {(item(1) + 1, item(0)(0), item(0)(1), item(0)(2))})
}