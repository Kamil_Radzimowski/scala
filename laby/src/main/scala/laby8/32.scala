@main
def yield_(): Unit = {
    print(threeNumbers(10))
}

def threeNumbers(n: Int): List[(Int, Int, Int)] = {
    return for {
        a <- Range(1, n).toList
        b <- Range(1, n)
        c <- Range(1, n)
        if (a*a + b*b == c*c)
    } yield (a, b, c)
}