@main
def mastermind(): Unit = {
    val secret = List(1, 3, 2, 2, 4, 5)
    val guess = List(2, 1, 2, 4, 7, 2)
    println(rate(secret, guess))
}

def rate(secret: List[Int], guess: List[Int]): (Int, Int) = {
    val black = secret.zip(guess).filter(pair => {pair(0) == pair(1)}).length
    val white = secret.zip(guess).filter(pair => {pair(0) != pair(1)}).unzip

    return (black, white(0).intersect(white(1)).length)
} 