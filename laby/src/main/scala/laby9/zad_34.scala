import scala.io.Source

@main
def zad_34(): Unit = {
    println(histogram(10))
}

def histogram(max: Int): String = {
    val dane1 = Source.fromFile("ogniem_i_mieczem.txt").toList
    val letterList = dane1.foldLeft(List[Char]())((acc, current) => {
        if(current.isLetter){
            acc :+ current.toLower
        }
        else{
            acc
        }
    })
    val grouped = letterList.groupBy((item) => item).toList.map((pair) => (pair(0), pair(1).length))
    val sortedGrouped = grouped.sortBy((pair) => {pair(1)}).reverse
    val scale = sortedGrouped(0)(1) / max
    val histogram = sortedGrouped.foldLeft("")((result, current) => {
        val starCount = (current(1) / scale).toFloat.floor
        result.concat(current(0).toString).concat(": ").concat("*" * starCount.toInt).concat("\n")
    })
    return histogram
}