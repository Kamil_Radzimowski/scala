import scala.io.Source

@main
def zad_33(): Unit = { 
    val dane2 = Source.fromFile("nazwiska.txt").getLines.toList
    println(weirdCounter(dane2))
}

def weirdCounter(data: List[String]): List[String] = {
    val maxNumberOfDifferentLetters = data.foldLeft((List[String](), 0, 0))((acc, current) => {
        val nameAndSurname = current.split(" ")
        val differentLetters = nameAndSurname(0).foldLeft(List[Char]())((list, currentChar) => {
            if(list.contains(currentChar.toLower)){
                list
            }
            else{
                list :+ currentChar.toLower
            }
        })
        if(differentLetters.length > acc(1)){
            (List(nameAndSurname(1)), differentLetters.length, nameAndSurname(1).length)
        }
        else if(differentLetters.length == acc(1)){
            if(nameAndSurname(1).length < acc(2)){
                (List(nameAndSurname(1)), differentLetters.length, nameAndSurname(1).length)
            }
            else if (nameAndSurname(1).length == acc(1)){
                (acc(0) :+ nameAndSurname(1), acc(1), acc(2))
            }
            else{
                acc
            }
        }
        else{
            acc
        }
    })
    return maxNumberOfDifferentLetters(0)
}