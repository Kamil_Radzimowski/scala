import scala.annotation.tailrec

@main
def zad_ciag(): Unit = {
    println(ciag(4))
}

def ciag(n: Int): Int ={

    @tailrec
    def helper(n: Int, a: Int = 1, b: Int = 1): Int = {
        n match {
            case 1 => a
            case 2 => b
            case _ => helper(n, b, a+b)
        }
    }
    return helper(n)
}

