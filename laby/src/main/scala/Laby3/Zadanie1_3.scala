import scala.annotation.tailrec

def reverse(str: String): String = {

    @tailrec
    def helper(str: String, acc: String, currentIndex: Int): String = {
        if(currentIndex < 0){
            return acc
        }
        //var accNew = acc
        //accNew = accNew + str.charAt(currentIndex).toString
        helper(str, acc + str.charAt(currentIndex).toString, currentIndex - 1)
    }
    return helper(str, "", str.length - 1)
}

@main
def zad_reverse(): Unit = {
    var napis = reverse("Kamil")   
    println(napis)
    println("Kamil")
}

