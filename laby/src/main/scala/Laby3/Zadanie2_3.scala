import scala.annotation.tailrec

@main
def zad_prime(): Unit = {
    var n = 7
    println(isPrime(n))
}


def isPrime(x:Integer): Boolean = {
    val y = math.floor(Math.sqrt(x.doubleValue()))
    primeHelper(x, y)
  }

  @tailrec
  private def primeHelper(x: Integer, y: Double): Boolean = {
    if (y >= x){
      return true
    }
    if((x%y == 0) && (y != 1)){
      return false
    }
    primeHelper(x, y+1)
  }