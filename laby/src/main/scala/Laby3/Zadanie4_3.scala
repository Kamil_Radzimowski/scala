import scala.annotation.tailrec

@main
def zad_tasuj(): Unit = {
    val list1 = List(2, 4, 3, 5)
    val list2 = List(1, 2, 2, 3, 1, 5)
    println(tasuj(list1, list2))
}

def tasuj_better(list1: List[Int], list2: List[Int]): List[Int] = {
    def helper(list1: List[Int], list2: List[Int], acc: List[Int]): List[Int] = {
        (list1, list2) match {
            case (Nil, Nil) => acc
            case (Nil, value :: tail) => helper(list1, tail, acc :+ value)
            case (value :: tail, Nil) => helper(tail, list2, acc :+ value)
            case (value1 :: tail1, value2 :: tail2) => if(value1 > value2){helper(tail1, list2, acc :+ value1)}else{helper(list1, tail2, acc :+ value2)}
        }
    }
    removeDuplicates(helper(list1, list2, List()))
}

def tasuj(list1: List[Int], list2: List[Int]): List[Int] = {

    @tailrec
    def helper(list1: List[Int], list2: List[Int], list1Index: Int, list2Index: Int, acc: List[Int]): List[Int] = {
        if (list1Index >= list1.size && list2Index >= list2.size){
            acc
        }
        else if (list1Index >= list1.size  && !(list2Index >= list2.size)){
            helper(list1, list2, list1Index, list2Index + 1, list2(list2Index) +: acc)
        }
        else if (!(list1Index >= list1.size)  && list2Index >= list2.size){
            helper(list1, list2, list1Index + 1, list2Index, list1(list1Index) +: acc)
        }
        else if(list2(list2Index) < list1(list1Index)){
            helper(list1, list2, list1Index, list2Index + 1, list2(list2Index) +: acc)
        }
        else{
            helper(list1, list2, list1Index + 1, list2Index, list1(list1Index) +: acc)
        }
    }
    removeDuplicates(helper(list1, list2, 0, 0, List())).reverse
}

def removeDuplicates(list: List[Int]): List[Int] = {

    @tailrec
    def helper(list: List[Int], index: Int, acc: List[Int]): List[Int] = {
        if (index > list.size - 1){
           return acc
        }
        if(index == list.size - 1){
           return  list(index) +: acc
        }
        if(list(index) != list(index + 1)){
           helper(list, index + 1, list(index) +: acc)
        }
        else{
            helper(list, index + 1, acc)
        }
    }

    return helper(list, 0, List()).reverse
}
